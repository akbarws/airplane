import java.util.*;

public class AirPlane {

    HashMap<String, Integer> price = new HashMap<String, Integer>();
    HashMap<String, Integer> typePlane = new HashMap<String, Integer>();

    private int laguage = 20;;
    private String [] kelas = {"Economy", "Business", "First Class"};

    private String nama;
    private String userRoute;
    private String userPlane;
    private String userClass;

    private void setNama(String nama){
        this.nama = nama;
    }

    private String getNama(){
        return this.nama;
    }

    private void setUserClass(String userClass){
        this.userClass = userClass;
    }

    private String getUserClass(){
        return this.userClass;
    }

    public String getUserRoute() {
        return userRoute;
    }

    public void setUserRoute(String userRoute){
        this.userRoute = userRoute;
    }

    public void setUserRouteFromChoice(int userChoice) {
        switch (userChoice){
            case 1 :
                setUserRoute("europe - asia");
                break;
            case 2 :
                setUserRoute("asia - europe");
                break;
            case 3 :
                setUserRoute("europe - usa");
                break;
        }
    }

    public String getUserPlane(){
        return this.userPlane;
    }

    public void setUserPlane(String userPlane){
        this.userPlane = userPlane;
    }

    public void setUserPlaneFromChoice(int userChoice) {
        switch (userChoice){
            case 1 :
                setUserPlane("Airbus 380");
                break;
            case 2 :
                setUserPlane("Boeing 747");
                break;
            case 3 :
                setUserPlane("Boeing 787");
                break;
        }
    }

    public int getKelasLength(){
        return this.kelas.length;
    }

    public String getKelas(int index){
        return this.kelas[index];
    }

    public float getFinalPrice(){
        float finalPrice = 0f;
        switch (this.getUserClass()){
            case "Economy" :
                finalPrice = 100 * price.get(this.getUserRoute()) / 100;
                break;
            case "Business" :
                finalPrice = 225 * price.get(this.getUserRoute()) / 100;
                break;
            case "First Class" :
                finalPrice = 300 * price.get(this.getUserRoute()) / 100;
                break;
        }
        return finalPrice;
    }

    public int getLaguage(){
        return this.laguage;
    }

    public int getFinalLaguage(){
        int finalLaguage = 0;
        switch (this.getUserClass()){
            case "Economy" :
                finalLaguage = 100 * this.getLaguage() / 100;
                break;
            case "Business" :
                finalLaguage = 200 * this.getLaguage() / 100;
                break;
            case "First Class" :
                finalLaguage = 250 * this.getLaguage() / 100;
                break;
        }
        return finalLaguage;
    }

    public void print(){
        System.out.println("Nama : "+this.getNama());
        System.out.println("Rute : "+this.getUserRoute());
        System.out.println("Tipe Pesawat : "+this.getUserPlane());
        System.out.println("Kelas : "+this.getUserClass());
        System.out.println("Harga : $"+this.getFinalPrice());
        System.out.println("Laguage : "+this.getFinalLaguage()+" kg");
    }

    AirPlane(){
        price.put("europe - asia", 500);
        price.put("asia - europe", 600);
        price.put("europe - us", 650);
        typePlane.put("Airbus 380", 600);
        typePlane.put("Boeing 747", 600);
        typePlane.put("Boeing 787", 650);
    }

    public void inputName(Scanner s){
        System.out.print("Masukkan nama anda:");
        this.setNama(s.nextLine());
        System.out.println("##################");
        int index = 0;
        for (Map.Entry rute : this.price.entrySet()) {
            System.out.println((index+1) + ". "+rute.getKey());
            index++;
        }
    }

    public void inputRute(Scanner s){
        System.out.print("Masukkan rute:");
        this.setUserRouteFromChoice(s.nextInt());
        System.out.println("##################");
        int index = 0;
        for (Map.Entry rute : this.typePlane.entrySet()) {
            System.out.println((index+1) + ". "+rute.getKey());
            index++;
        }
    }

    public void inputPesawat(Scanner s){
        System.out.print("Masukkan pesawat:");
        this.setUserPlaneFromChoice(s.nextInt());
        System.out.println("##################");
        int index = 0;
        for(index=0;index<this.getKelasLength();index++){
            System.out.println((index+1)+". "+this.getKelas(index));
        }
        System.out.print("Masukkan kelas:");
        this.setUserClass(this.getKelas(s.nextInt()-1));
    }

    public static void main(String[] args){
        Scanner s = new Scanner(System.in);
        AirPlane a = new AirPlane();
        a.inputName(s);
        a.inputRute(s);
        a.inputPesawat(s);
        a.print();
    }
}
